package ru.tsk.ilina.tm.api.service;

import ru.tsk.ilina.tm.enumerated.Status;
import ru.tsk.ilina.tm.model.Project;
import ru.tsk.ilina.tm.model.Task;

import java.util.List;

public interface ITaskService {

    void create(String name, String description);

    void add(Task task);

    void remove(Task task);

    void clear();

    List<Task> findAll();

    Task removeByID(String id);

    Task removeByIndex(Integer index);

    Task removeByName(String name);

    Task findByID(String id);

    Task findByIndex(Integer index);

    Task findByName(String name);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task startByID(String id);

    Task startByIndex(Integer index);

    Task startByName(String name);

    Task finishByID(String id);

    Task finishByIndex(Integer index);

    Task finishByName(String name);

    Task changeStatusByIndex(Integer index, Status status);

    Task changeStatusByName(String name, Status status);

    Task changeStatusByID(String id, Status status);

}
